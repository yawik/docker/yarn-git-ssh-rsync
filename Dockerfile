FROM node:20-slim
LABEL maintainer "Carsten Bleek <bleek@cross-solution.de>"

ARG REFRESHED_AT
ENV REFRESHED_AT $REFRESHED_AT

RUN apt-get -qq update
RUN apt-get -qq upgrade
RUN apt-get -qq install \
    git \
    rsync \
    ssh \
    jq \
    curl


RUN yarn global add pm2
RUN yarn global add jest
